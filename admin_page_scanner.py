import urllib.request
import urllib.response
import string
import sys
import os




url = input("Please enter the target (example: http://www.google.com/): ")
print("Target URL is: ",url)
try:
    urltest = urllib.request.urlopen(url)
except:
    print("Invalid target URL")
    os.system("pause")
    sys.exit()

print("Target URL is valid, status code is",urltest.getcode())

status = 0
count = 0
admlist = [line.strip() for line in open("admlist.txt", 'r')]
adm = []

while 1:
    try:
        target = url + admlist[count]
    except:
        print("Admin pages found:",adm)
        os.system("pause")
        sys.exit()
    try:
        count = count+1
        target_open = urllib.request.urlopen(target)
        status = target_open.getcode()
        print("[" + str(count) + "]" + " | " + target + " seems to exist")
        adm.append(target)

    except urllib.error.HTTPError as errorstatus:
        status = errorstatus.code
        print("[" + str(count) + "]" + " | " + target + " does not exist: error " + str(errorstatus.code))
    except:
        print("Please add '/' at the end of the URL")
        os.system("pause")
        sys.exit()
    








